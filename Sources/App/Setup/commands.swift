import Fluent

import AccountAPI
import TodoAPI

public func commands(config: inout CommandConfig) {
    config.useFluentCommands()

    AccountAPI.commands(config: &config, monolith: true)
    TodoAPI.commands(config: &config, monolith: true)
}
