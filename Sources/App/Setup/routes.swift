import Shared
import Vapor

import AccountAPI
import TodoAPI

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    addHealthCheck(to: router)

    try AccountAPI.routes(router, monolith: true)
    try TodoAPI.routes(router, monolith: true)
}
