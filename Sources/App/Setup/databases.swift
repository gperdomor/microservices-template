import Fluent

import AccountAPI
import TodoAPI

public func databases(config: inout DatabasesConfig) throws {
    try AccountAPI.databases(config: &config)
    try TodoAPI.databases(config: &config, monolith: true)
}
