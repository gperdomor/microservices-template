import Fluent

import AccountAPI
import TodoAPI

public func migrate(config: inout MigrationConfig) {
    AccountAPI.migrate(config: &config)
    TodoAPI.migrate(config: &config, monolith: true)
}
