import Shared
import Vapor

/// Register your application's middlewares here.
public func middlewares(config: inout MiddlewareConfig) throws {
    try Shared.middlewares(config: &config)
}
