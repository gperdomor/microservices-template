import VaporExt

/// Controls basic CRUD operations on `Todo`s.
final class TodoController: RouteCollection {
    func boot(router: Router) throws {
        let todos = router.grouped("todos")

        todos.get(use: index)
        todos.post(Todo.self, use: create)
        todos.delete(Todo.parameter, use: delete)
    }

    /// Returns a list of all `Todo`s.
    func index(_ req: Request) throws -> Future<[Todo]> {
        let criteria: [FilterOperator<Todo.Database, Todo>] = try [
            req.filter(\Todo.title, at: "title")
        ].compactMap { $0 }

        var sort: [Todo.Database.QuerySort] = try [
            req.sort(\Todo.title, as: "title"),
            req.sort(\Todo.createdAt, as: "created_at")
        ].compactMap { $0 }

        if sort.isEmpty {
            let defaultSort = Todo.Database.querySort(Todo.Database.queryField(.keyPath(\Todo.createdAt)), .ascending)
            sort.append(defaultSort)
        }

        return Todo.find(by: criteria, sortBy: sort, on: req)
    }

    /// Saves a decoded `Todo` to the database.
    func create(_ req: Request, todo: Todo) throws -> Future<Todo> {
        return todo.save(on: req)
    }

    /// Deletes a parameterized `Todo`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Todo.self).flatMap { todo in
            todo.delete(on: req)
        }.transform(to: .ok)
    }
}
