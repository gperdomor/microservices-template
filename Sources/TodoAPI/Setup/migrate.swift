import Fluent

/// Register your model's migrations here.
public func migrate(config: inout MigrationConfig, monolith: Bool = false) {
    config.add(model: Todo.self, database: getIdentifier(monolith))
}
