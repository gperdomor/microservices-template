import Vapor

/// Register your application's commands here.
public func commands(config: inout CommandConfig, monolith: Bool = false) {
    if !monolith {
        config.useFluentCommands()
    }
}
