import FluentPostgreSQL
import Vapor

/// Register your application's databases here.
public func databases(config: inout DatabasesConfig, monolith: Bool = false) throws {
    let todosUrl = Environment.get("TODOS_DATABASE_URL", "postgres://vapor:password@localhost:5442/todos")

    guard let todosConfig = PostgreSQLDatabaseConfig(url: todosUrl) else { throw Abort(.internalServerError) }

    /// Register the databases
    let todosDB = PostgreSQLDatabase(config: todosConfig)

    config.add(database: todosDB, as: getIdentifier(monolith))

    if Environment.get("TODOS_DATABASE_LOGS", false) {
        config.enableLogging(on: getIdentifier(monolith))
    }
}

extension DatabaseIdentifier {
    static var todos: DatabaseIdentifier<PostgreSQLDatabase> {
        return .init("todos")
    }
}

func getIdentifier(_ monolith: Bool) -> DatabaseIdentifier<PostgreSQLDatabase> {
    return monolith ? .todos : .psql
}
