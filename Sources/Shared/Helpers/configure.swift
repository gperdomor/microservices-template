import Vapor

/// Register health route.
public func addHealthCheck(to router: Router) {
    let health = router.grouped("_health", "status")

    health.get { req -> Response in
        return req.response("It works!", as: .json)
    }
}

/// Register your application's middlewares here.
public func middlewares(config: inout MiddlewareConfig) throws {
    /// config.use(FileMiddleware.self) // Serves files from `Public/` directory
    config.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    // Other Middlewares...
}
