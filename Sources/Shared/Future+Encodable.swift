import Vapor

public extension Future where T: Encodable {
    public func with(status _: HTTPStatus, on req: Request) throws -> Future<Response> {
        return map(to: Response.self) { encodable in
            let response = req.response()
            try response.content.encode(json: encodable)
            response.http.status = .created
            return response
        }
    }
}
