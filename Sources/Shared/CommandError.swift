import Vapor

extension CommandError {
    public init(identifier: String, reason: String, source: SourceLocation) {
        self.identifier = identifier
        self.reason = reason
        sourceLocation = source
        stackTrace = CommandError.makeStackTrace()
    }
}
