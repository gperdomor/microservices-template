import Crypto
import VaporExt

struct CreateUserCommand: Command {
    static let command = "user:create"

    var arguments: [CommandArgument] {
        return []
    }

    var options: [CommandOption] {
        return []
    }

    var help: [String] {
        return [
            "The \(CreateUserCommand.command) command creates a user:",
            "vapor run \(CreateUserCommand.command)"
        ]
    }

    func run(using context: CommandContext) throws -> EventLoopFuture<Void> {
        let console = context.console

        console.info("Welcome to the User generator")

        let name = console.ask("👶  What's is the user's name?")
        let username = console.ask("📫  What's is the user's email?")
        let password = console.ask("🔒  Please choose a password", isSecure: true)
        let passwordConfirm = console.ask("🔐  Please confirm the password", isSecure: true)

        guard password == passwordConfirm else {
            throw CommandError(
                identifier: "usernameTaken",
                reason: "The username '\(username)' already exists.",
                source: .capture()
            )
        }

        let line = String(repeating: "-", count: console.size.width)

        console.print(line, newLine: true)
        console.info("Now the user will be generated with the following parameters:", newLine: true)
        console.print("👶  Name: \(name)", newLine: true)
        console.print("📫  Email: \(username)", newLine: true)
        console.print(line, newLine: true)

        let result = console.confirm("✅  Proceed?".consoleText(.warning))

        guard result == true else { return context.container.future() }

        let user = User(name: name, username: username, password: password)

        console.info("🚀  Starting user creation...", newLine: true)

        console.info("👉  Validating parameters...", newLine: true)
        try user.validate()
        console.info("✅  Done", newLine: true)

        return context.container.withNewConnection(to: .psql) { db in
            console.info("👉  Looking for duplicates...", newLine: true)

            return try User
                .count(by: [\.username == username], on: db)
                .equal(to: 0, or: CommandError(
                    identifier: "usernameTaken",
                    reason: "The username '\(username)' already exists.",
                    source: .capture()
                ))
                .flatMap(to: Void.self) { _ in
                    console.info("✅  Done", newLine: true)

                    console.info("👉  Saving to the database...", newLine: true)

                    user.password = try BCrypt.hash(user.password)

                    return user.create(on: db).map { _ in
                        console.info("✅  Done", newLine: true)
                        console.success("All done! 🎉  \(username) has been created.", newLine: true)
                        return ()
                    }
                }
        }
    }
}
