import VaporExt

struct ActivateUserCommand: Command {
    static let command = "user:activate"

    var arguments: [CommandArgument] {
        return [
            .argument(name: "username", help: ["The username"])
        ]
    }

    var options: [CommandOption] {
        return []
    }

    var help: [String] {
        return [
            "The \(ActivateUserCommand.command) command activates a user (so they will be able to log in):",
            "vapor run \(ActivateUserCommand.command) example@domain.com"
        ]
    }

    func run(using context: CommandContext) throws -> EventLoopFuture<Void> {
        let console = context.console

        let username = try context.argument("username")

        let line = String(repeating: "-", count: console.size.width)

        console.print(line, newLine: true)
        console.info("Now the user \(username) will be activated", newLine: true)
        console.print(line, newLine: true)

        let result = console.confirm("✅  Proceed?".consoleText(.warning))

        guard result == true else {
            return Future.map(on: context.container) { () }
        }

        console.info("🚀  Starting user activation...", newLine: true)

        return context.container.withNewConnection(to: .psql) { db in
            console.info("👉  Looking for \(username)...", newLine: true)

            return User
                .findOne(by: [\.username == username], on: db)
                .unwrap(or: CommandError(
                    identifier: "unknownUsername",
                    reason: "The user with username '\(username)' not exist",
                    source: .capture()
                ))
                .flatMap(to: Void.self) { user in
                    console.info("✅  Done", newLine: true)

                    user.enabled = true

                    console.info("👉  Saving to the database...", newLine: true)

                    return user.save(on: db).map(to: Void.self) { _ in
                        console.info("✅  Done", newLine: true)
                        console.success("All done! 🎉  \(username) has been activated.", newLine: true)
                        return ()
                    }
                }
        }
    }
}
