import Crypto
import VaporExt

struct ChangeUserPasswordCommand: Command {
    static let command = "user:change-password"

    var arguments: [CommandArgument] {
        return [
            .argument(name: "username", help: ["The username"])
        ]
    }

    var options: [CommandOption] {
        return []
    }

    var help: [String] {
        return [
            "The \(ChangeUserPasswordCommand.command) command changes the password of a user:",
            "vapor run \(ChangeUserPasswordCommand.command) example@domain.com new-password"
        ]
    }

    func run(using context: CommandContext) throws -> EventLoopFuture<Void> {
        let console = context.console

        let username = try context.argument("username")

        let password = console.ask("🔒  Please choose a password", isSecure: true)
        let passwordConfirm = console.ask("🔐  Please confirm the password", isSecure: true)

        guard password == passwordConfirm else {
            console.error("❌  Passwords don't match.", newLine: true)
            return context.container.future()
        }

        let line = String(repeating: "-", count: console.size.width)

        console.print(line, newLine: true)
        console.info("Now the user \(username) password will be updated", newLine: true)
        console.print(line, newLine: true)

        let result = console.confirm("✅  Proceed?".consoleText(.warning))

        guard result == true else {
            return context.container.future()
        }

        console.info("🚀  Starting user update...", newLine: true)

        return context.container.withNewConnection(to: .psql) { db in
            console.info("👉  Looking for \(username)...", newLine: true)

            return User
                .findOne(by: [\.username == username], on: db)
                .unwrap(or: CommandError(
                    identifier: "unknownUsername",
                    reason: "The user with username '\(username)' not exist",
                    source: .capture()
                ))
                .flatMap(to: Void.self) { user in
                    console.info("✅  Done", newLine: true)

                    let hashedPassword = try BCrypt.hash(password)

                    user.password = hashedPassword

                    console.info("👉  Saving to the database...", newLine: true)

                    return user.save(on: db).map(to: Void.self) { _ in
                        console.info("✅  Done", newLine: true)
                        console.success("All done! 🎉  \(username) password has been changed.", newLine: true)
                        return ()
                    }
                }
        }
    }
}
