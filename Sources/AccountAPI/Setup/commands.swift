import Vapor

/// Register your application's commands here.
public func commands(config: inout CommandConfig, monolith: Bool = false) {
    if !monolith {
        config.useFluentCommands()
    }

    // User Commands
    config.use(CreateUserCommand(), as: CreateUserCommand.command)
    config.use(ChangeUserPasswordCommand(), as: ChangeUserPasswordCommand.command)
    config.use(ActivateUserCommand(), as: ActivateUserCommand.command)
    config.use(DeactivateUserCommand(), as: DeactivateUserCommand.command)
}
