import Fluent

/// Register your model's migrations here.
public func migrate(config: inout MigrationConfig) {
    config.add(model: User.self, database: .psql)
    config.add(model: UserToken.self, database: .psql)
}
