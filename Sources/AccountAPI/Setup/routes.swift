import Shared
import Vapor

/// Register your application's routes here.
public func routes(_ router: Router, monolith: Bool = false) throws {
    if !monolith {
        addHealthCheck(to: router)
    }

    try router.register(collection: AuthController())
    try router.register(collection: UserController())
}
