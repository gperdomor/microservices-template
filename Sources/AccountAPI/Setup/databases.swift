import FluentPostgreSQL
import Vapor

/// Register your application's databases here.
public func databases(config: inout DatabasesConfig) throws {
    let accountsUrl = Environment.get("ACCOUNTS_DATABASE_URL", "postgres://vapor:password@localhost:5441/accounts")

    guard let accountsConfig = PostgreSQLDatabaseConfig(url: accountsUrl) else { throw Abort(.internalServerError) }

    /// Register the databases
    let accountsDB = PostgreSQLDatabase(config: accountsConfig)

    config.add(database: accountsDB, as: .psql)

    if Environment.get("ACCOUNTS_DATABASE_LOGS", false) {
        config.enableLogging(on: .psql)
    }
}
