import Vapor

struct LoginResponse: Content {
    var accessToken: String
    var user: User.PublicUser

    init(accessToken: String, user: User) {
        self.accessToken = accessToken
        self.user = user.public
    }
}
