import Authentication
import Crypto
import FluentPostgreSQL
import Vapor

/// An ephermal authentication token that identifies a registered user.
final class UserToken: Codable {
    /// Token's unique identifier.
    var id: UUID?

    /// Unique token string.
    var string: String

    /// Reference to user that owns this token.
    var userID: User.ID

    /// Expiration date. Token will no longer be valid after this point.
    var expiresAt: Date?

    /// Creates a new `UserToken`.
    init(id: UUID? = nil, string: String, userID: User.ID) {
        self.id = id
        self.string = string
        // set token to expire after 5 hours
        expiresAt = Date(timeInterval: 60 * 60 * 5, since: .init())
        self.userID = userID
    }
}

extension UserToken: PostgreSQLUUIDModel {
    static var entity = "tokens"

    /// See `Model`.
    static var deletedAtKey: TimestampKey? = \.expiresAt
}

// Allows `UserToken` to be used as a Fluent migration.
extension UserToken: Migration {
    /// See `Migration`.
    static func prepare(on conn: PostgreSQLConnection) -> Future<Void> {
        return Database.create(UserToken.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.string)
            builder.field(for: \.userID)
            builder.field(for: \.expiresAt)

            builder.unique(on: \.string)

            builder.reference(from: \.userID, to: \User.id)
        }
    }
}

// MARK: Relations

extension UserToken {
    /// Fluent relation to the user that owns this token.
    var user: Parent<UserToken, User> {
        return parent(\.userID)
    }
}

extension UserToken: BearerAuthenticatable {
    static var tokenKey: WritableKeyPath<UserToken, String> = \UserToken.string
}

extension UserToken: Authentication.Token {
    typealias UserType = User
    typealias UserIDType = User.ID

    static var userIDKey: WritableKeyPath<UserToken, UUID> = \UserToken.userID
}

extension UserToken {
    /// Creates a new `UserToken` for a given user.
    static func generate(for user: User, length: Int = 16) throws -> UserToken {
        return try .generate(userID: user.requireID(), length: length)
    }

    /// Creates a new `UserToken` for a given user.
    static func generate(userID: User.ID, length: Int = 16) throws -> UserToken {
        // generate a random 128-bit, base64-encoded string.
        let string = try CryptoRandom().generateData(count: length).base64EncodedString()
        // init a new `UserToken` from that string.
        return .init(string: string, userID: userID)
    }
}

extension UserToken: Equatable {
    public static func == (lhs: UserToken, rhs: UserToken) -> Bool {
        return lhs.string == rhs.string
    }
}
