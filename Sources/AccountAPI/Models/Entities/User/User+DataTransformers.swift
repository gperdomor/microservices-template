import FluentPostgreSQL
import Vapor

extension User {
    public final class PublicUser: Content, PostgreSQLUUIDModel {
        var id: User.ID?
        var name: String
        var username: String
        var enabled: Bool

        init(user: User) {
            id = user.id
            name = user.name
            username = user.username
            enabled = user.enabled
        }
    }

    public var `public`: PublicUser {
        return PublicUser(user: self)
    }
}
