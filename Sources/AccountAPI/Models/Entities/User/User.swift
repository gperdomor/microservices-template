import Authentication
import FluentPostgreSQL
import Validation
import Vapor

final class User: Codable {
    var id: UUID?

    var name: String
    var username: String
    var password: String
    var enabled: Bool

    var createdAt: Date?
    var updatedAt: Date?
    var deletedAt: Date?

    init(name: String, username: String, password: String, enabled: Bool = true) {
        self.name = name
        self.username = username
        self.password = password
        self.enabled = enabled
    }
}

extension User: Validatable {
    static func validations() throws -> Validations<User> {
        var validations = Validations(User.self)
        try validations.add(\.name, .characterSet(.letters + .whitespaces) && .count(2...))
        try validations.add(\.username, .email)
        try validations.add(\.password, .ascii && .count(8...))
        return validations
    }
}

/// Allows `User` to be used as Fluent model.
extension User: PostgreSQLUUIDModel {
    static var entity = "users"

    static var createdAtKey: TimestampKey? = \.createdAt
    static var deletedAtKey: TimestampKey? = \.deletedAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}

/// Allows `User` to be used as a dynamic migration.
extension User: Migration {
    static func prepare(on connection: PostgreSQLConnection) -> Future<Void> {
        return Database.create(self, on: connection) { builder in
            try addProperties(to: builder)
            builder.unique(on: \.username)
        }
    }
}

/// Allows `User` to be encoded to and decoded from HTTP messages.
extension User: Content {}

/// Allows `User` to be used as a dynamic parameter in route definitions.
extension User: Parameter {}

// MARK: Authentication

extension User: PasswordAuthenticatable {
    static var usernameKey: WritableKeyPath = \User.username
    static var passwordKey: WritableKeyPath = \User.password

    static func authenticate(using basic: BasicAuthorization, verifier: PasswordVerifier, on conn: DatabaseConnectable) -> Future<User?> {
        return User.query(on: conn)
            .filter(usernameKey == basic.username)
            .first()
            .map(to: User?.self) { user in
                guard let user = user, try verifier.verify(basic.password, created: user.basicPassword) else {
                    return nil
                }

                return user
            }
    }
}

extension User: TokenAuthenticatable {
    typealias TokenType = UserToken
}
