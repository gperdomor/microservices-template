import Crypto
import VaporExt

/// Controls basic CRUD operations on `User`s.
final class UserController: RouteCollection {
    func boot(router: Router) throws {
        let users = router.grouped("users")

        users.get(use: index)
        users.get(User.parameter, use: show)

        users.post(User.self, at: "", use: create)
        users.post(User.parameter, "activate", use: activate)
        users.post(User.parameter, "deactivate", use: deactivate)

        users.put(User.self, at: User.parameter, use: update)

        users.delete(User.parameter, use: delete)
    }

    /// Returns a list of all `User`s.
    func index(_ req: Request) throws -> Future<[User.PublicUser]> {
        let criteria: [FilterOperator<User.Database, User>] = try [
            req.filter(\User.name, at: "name"),
            req.filter(\User.username, at: "username"),
            req.filter(\User.enabled, at: "enabled")
        ].compactMap { $0 }

        var sort: [User.Database.QuerySort] = try [
            req.sort(\User.name, as: "name"),
            req.sort(\User.username, as: "username"),
            req.sort(\User.createdAt, as: "created_at")
        ].compactMap { $0 }

        if sort.isEmpty {
            let defaultSort = User.Database.querySort(User.Database.queryField(.keyPath(\User.name)), .ascending)
            sort.append(defaultSort)
        }
        
        return User.find(as: User.PublicUser.self, by: criteria, sortBy: sort, on: req)
    }

    /// Saves a decoded `User` to the database.
    func create(_ req: Request, payload: User) throws -> Future<Response> {
        return try User
            .count(by: [\User.username == payload.username], on: req)
            .equal(to: 0, or: Abort(.conflict, reason: "username \(payload.username) is taken"))
            .createUser(using: payload, on: req)
            .with(status: .ok, on: req)
    }

    /// Updates parameterized `User`.
    func update(_ req: Request, payload: User) throws -> Future<User.PublicUser> {
        return try req.parameters.next(User.self).flatMap(to: User.self) { user in
            try User.count(by: [
                \User.id != user.requireID(),
                \User.username == payload.username
            ], on: req)
                .equal(to: 0, or: Abort(.conflict, reason: "username \(payload.username) is taken"))
                .flatMap(to: User.self) { _ in
                    user.username = payload.username
                    user.enabled = payload.enabled
                    return user.update(on: req)
                }
        }.map(to: User.PublicUser.self) { user in
            user.public
        }
    }

    /// Shows a parameterized `User`.
    func show(_ req: Request) throws -> Future<User.PublicUser> {
        return try req.parameters.next(User.self).map(to: User.PublicUser.self) { user in
            user.public
        }
    }

    /// Deletes a parameterized `User`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(User.self).flatMap { user in
            user.delete(on: req)
        }.transform(to: .noContent)
    }

    /// Activates a parameterized `User`.
    func activate(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(User.self).flatMap(to: User.self) { user in
            user.enabled = true
            return user.update(on: req)
        }.transform(to: .noContent)
    }

    /// Deactivates a parameterized `User`.
    func deactivate(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(User.self).flatMap(to: User.self) { user in
            user.enabled = false
            return user.update(on: req)
        }.transform(to: .noContent)
    }
}

fileprivate extension Future where T == Bool {
    func createUser(using payload: User, on req: Request) throws -> Future<User.PublicUser> {
        return flatMap(to: User.PublicUser.self) { _ in
            let user = User(
                name: payload.name,
                username: payload.username,
                password: try BCrypt.hash(payload.password),
                enabled: payload.enabled
            )

            return user.create(on: req).map { savedUser in
                savedUser.public
            }
        }
    }
}
