import Crypto
import Fluent
import Vapor

struct AuthController: RouteCollection {
    func boot(router: Router) throws {
        router.post(LoginRequest.self, at: "login", use: login)

        let protected = router.grouped(User.tokenAuthMiddleware(), User.guardAuthMiddleware())

        protected.post("logout", use: logout)
    }

    func login(_ req: Request, loginRequest: LoginRequest) throws -> Future<LoginResponse> {
        let passwordVerifier = try req.make(BCryptDigest.self)

        return try User
            .authenticate(username: loginRequest.username, password: loginRequest.password, using: passwordVerifier, on: req)
            .unwrap(or: Abort(.unauthorized, reason: "Invalid credentials."))
            .deleteExpiredTokens(on: req)
            .createToken(on: req)
    }

    func logout(_ req: Request) throws -> Future<HTTPStatus> {
        guard let token = req.http.headers.bearerAuthorization else {
            throw Abort(.badRequest)
        }

        return UserToken.query(on: req)
            .filter(\UserToken.string == token.token)
            .delete()
            .transform(to: .noContent)
    }
}

extension Future where T == User {
    func createToken(on req: Request) throws -> Future<LoginResponse> {
        return flatMap(to: LoginResponse.self) { user in
            let token = try UserToken.generate(for: user, length: 18)

            return token.save(on: req).map { _ in
                LoginResponse(accessToken: token.string, user: user)
            }
        }
    }

    func deleteExpiredTokens(on req: Request) throws -> Future<User> {
        return flatMap(to: User.self) { user in
            let userId = try user.requireID()

            return UserToken.query(on: req)
                .filter(\.userID == userId)
                .filter(\.expiresAt < Date())
                .delete()
                .transform(to: user)
        }
    }
}
