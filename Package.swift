// swift-tools-version:4.1
import PackageDescription

let package = Package(
    name: "microservices-template",
    products: [
        .executable(name: "Run", targets: ["Run"]),
        .executable(name: "AccountAPI", targets: ["RunAccountAPI"]),
        .executable(name: "TodoAPI", targets: ["RunTodoAPI"]),
    ],
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        // 🐘 Swift ORM (queries, models, relations, etc) built on PostgreSQL.
        .package(url: "https://github.com/vapor/fluent-postgresql.git", from: "1.0.0"),
        // 👤 Authentication and Authorization layer for Fluent.
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.0"),
        // ⚙️ A collection of Swift extensions for wide range of Vapor data types and classes.
        .package(url: "https://github.com/vapor-community/vapor-ext.git", from: "0.3.0"),

        // MARK: Testing Packages

        // 🛠 Tools for testing Vapor apps easier
        .package(url: "https://gitlab.com/gperdomor/vapor-test-helpers.git", from: "0.1.1"),
    ],
    targets: [
        .target(name: "Shared", dependencies: ["Vapor"]),

        // Auth API
        .target(name: "AccountAPI", dependencies: ["FluentPostgreSQL", "Vapor", "VaporExt", "Shared", "Authentication"]),
        .target(name: "RunAccountAPI", dependencies: ["AccountAPI"]),
        .testTarget(name: "AccountAPITests", dependencies: ["AccountAPI", "VaporTestHelpers"]),

        // Todo API
        .target(name: "TodoAPI", dependencies: ["FluentPostgreSQL", "Vapor", "VaporExt", "Shared"]),
        .target(name: "RunTodoAPI", dependencies: ["TodoAPI"]),
        .testTarget(name: "TodoAPITests", dependencies: ["TodoAPI", "VaporTestHelpers"]),

        // Monolith App
        .target(name: "App", dependencies: ["FluentPostgreSQL", "Vapor", "AccountAPI", "TodoAPI"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App", "VaporTestHelpers"]),
    ]
)
