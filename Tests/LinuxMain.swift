import XCTest

@testable import AccountAPITests
@testable import AppTests
@testable import TodoAPITests

var tests = [XCTestCaseEntry]()
tests += AppTests.allTests()
tests += AccountAPITests.allTests()
tests += TodoAPITests.allTests()

XCTMain(tests)
